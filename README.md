
# Concepts

## Sequences

Sequence Data Types are used to store data in **containers** in the Python computer
language. The different types of containers used to store the data are List,
Tuple, and String. Lists are mutable and can hold data of any type, whereas
Strings are immutable and can only store data of the string type.

## Iteration

The process of taking an item from something e.g a list. When we use a loop to
loop over something it is called iteration.

**HW:** Read first 4 pages
[here](https://www.webpages.uidaho.edu/~stevel/504/Python%20Notes.pdf). Make
sure you understand everything (ask if anything unclear). Covers: Tuples,
String, List, Dict

## Conditionals

Conditionals (that is, conditional statements, conditional expressions and
conditional constructs,) are programming language commands for handling
decisions. Specifically, conditionals perform different computations or actions
depending on whether a programmer-defined boolean condition evaluates to true
or false.

**HW:** Read page 2
[here](https://www.webpages.uidaho.edu/~stevel/504/Python%20Notes.pdf). Make
sure you understand everything (ask if anything unclear). Covers `If`, `For`,
`While`, `Ternary`.

## Functions

A function is simply a “chunk” of code that you can use over and over again,
rather than writing it out multiple times. Functions enable programmers to
break down or decompose a problem into smaller chunks, each of which performs a
particular task.

**HW:** Read page 5
[here](https://www.webpages.uidaho.edu/~stevel/504/Python%20Notes.pdf). Make
sure you understand everything (ask if anything unclear).

## Reading/ writing Files

**HW:** Read [here](https://realpython.com/read-write-files-python/). Make
sure you understand everything (ask if anything unclear).

```
# Reading:
# 'with' specifies a context manager. Ensures file is closed once context finishes
with open(filename, mode='r', encoding='utf-8') as f:
    for line in f:
        print(line)
```

The above is equivalent to
```
f = open(filename, mode='r', encoding='utf-8')
try:
    # Further file processing goes here
finally: # success or exception
    f.close()
```

Writing files handled in similar manner

# References

* **Documentation:** [Official doc](https://docs.python.org/3/contents.html)
* **Tutorials:** [Official tutorial](https://docs.python.org/3/tutorial/),
  [Intermediate tutorial](https://book.pythontips.com/en/latest/index.html#)
* **Cheat sheets:** [Basic cheat
  sheet](https://www.webpages.uidaho.edu/~stevel/504/Python%20Notes.pdf), [Adv.
  cheat sheet](https://gto76.github.io/python-cheatsheet/)

