#!/usr/bin/env python3

def main():
    # create: 1) empty string to store output to write to file (report_output);
    #         2) variables to store calculations (num_days_above_60, num_days_above_50_but_not_60, num_days_recorded)
    # ....

    # open 'temps.txt' for reading lines
    with open('./temps.txt', mode='r', encoding='utf-8') as f:
        # ....

        # iterate over lines of file 'temps.txt'
        for line in f:
            # ....
            # create variables month_name and month_vals containing name of month as string and temperature values as list
            # print(month_name, month_vals)

            # calculate month_temp_avg
            # ....

            # update report_output with month_temp_avg
            # ....

            # update (num_days_above_60, num_days_above_50_but_not_60, num_days_recorded)
            # ....


    # update report_output with (num_days_above_60, num_days_above_50_but_not_60, num_days_recorded)
    # ....

    # write report_output to file 'tempReport.txt'
    with open('./tempReport.txt', mode='w', encoding='utf-8') as f:
        # ....

if __name__ == '__main__':
    main()
